def add(n1, n2)
  n1+n2
end

def subtract(n1, n2)
  n1-n2
end

def sum(num_arr)
  num_arr.length > 0 ? num_arr.reduce(:+) : 0
end

def multiply(n1, n2)
  n1*n2
end

#Ruby does not support overloading
def multiply_array(num_arr)
  num_arr.reduce(:*)
end

def power(n1, n2)
  n1**n2
end

def factorial(n)
  (1..n).reduce(:*) || 1
end
