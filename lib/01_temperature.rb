#Convert fahrenheit to celsius
def ftoc(fah)
  ((fah-32)/1.8).round(1)
end

#Convert celsius to fahrenheit
def ctof(cel)
  (cel*1.8+32).round(1)
end
