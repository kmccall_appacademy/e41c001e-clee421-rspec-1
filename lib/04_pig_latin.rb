def translate(string)
  string.split.map {|word| latinize(word)}.join(" ")
end

def latinize(word)
  word.chars.rotate(first_vowel_idx(word)).join + "ay"
end

def first_vowel_idx(word)
  vowel = ["a", "e", "i", "o", "u"]
  #replace all "qu" with a consonant as it acts as a consonant
  word.gsub("qu", "cc").each_char.with_index {|c, i| return i if vowel.include?(c)}
  return 0
end
