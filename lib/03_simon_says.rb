def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, n = 2)
  arr = []
  n.times {|num| arr << string}
  arr.join(' ')
end

def start_of_word(string, n)
  string[0...n]
end

def first_word(string)
  string.split.first
end

def titleize(string)
  #add to this list as necessary
  lit_word_arr = ["the", "of", "and", "is", "over"]
  title = string.split.map do |word|
    lit_word_arr.include?(word.downcase) ? word : word.capitalize
  end.join(' ')
  title[0] = title[0].upcase
  title
end
